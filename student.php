<? include "./admin_chk.php"; ?>
<? include "./db_connect.php"; ?>

<?
	$query = "select count(*) as cnt from student";
	$result = mysql_query($query, $connect) or die(mysql_error());
	$row = mysql_fetch_array($result);
	
	function selected($type, $value)
	{
		if($_POST[$type] == $value) return " selected";
	}
?>

<? include "./header.php"; ?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">학생 목록</h1>
				<div class="table-responsive">
					<table class="table table-striped">
						<tr>
							<td width="80%">
								<form name="frm_search_type1" action="./student.php" method="post">
									<select name="type1">
										<option value="stu_num"<?=selected("type1", "stu_num");?>>학번</option>
										<option value="stu_name"<?=selected("type1", "stu_name");?>>이름</option>
									</select>
									<input type="text" name="keyword" value="<?=$_POST['keyword']?>" size="15" />
									<button type="submit" class="btn btn-xs btn-default">검색</button>
								</form>
							</td>
							<th width="20%">[ 학생 수 : <?=number_format($row[cnt]);?> 명 ]</th>
						</tr>
					</table>
				</div>
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>학번</th>
								<th>이름</th>
								<th>연락처</th>
								<th>동아리 가입 수</th>
								<th>가입 동아리 목록</th>
								<th>관리</th>
							</tr>
						</thead>
						<tbody>
<?
	$query = "select * from student";
	
	if($_POST['type1'] == "stu_num") $query .= " where student.stu_num like '%".$_POST['keyword']."%'";
	else if($_POST['type1'] == "stu_name") $query .= " where student.stu_name like '%".$_POST['keyword']."%'";

	$result1 = mysql_query($query, $connect) or die(mysql_error());
	while($row1 = mysql_fetch_array($result1))
	{
		$query = "select count(*) as cnt from member where stu_num = '".$row1[stu_num]."' and mbr_leave_check = false";
		$result2 = mysql_query($query, $connect) or die(mysql_error());
		$row2 = mysql_fetch_array($result2);
?>
							<tr>
								<td><?=$row1[stu_num]?></td>
								<td><?=$row1[stu_name]?></td>
								<td><?=$row1[stu_contact]?></td>
								<td><?=number_format($row2[cnt]);?> 개</td>
								<td>
									<form name="frm_info" action="./student_info.php" method="post">
										<input type="hidden" name="stu_num" value="<?=$row1[stu_num]?>" />
										<button type="submit" class="btn btn-sm btn-default">보기</button>
									</form>
								</td>
								<td>
									<form name="frm_manage" action="./student_manage.php" method="post">
										<input type="hidden" name="stu_num" value="<?=$row1[stu_num]?>" />
										<button type="submit" class="btn btn-sm btn-default">관리</button>
									</form>
								</td>
							</tr>
<?
	}
?>
						</tbody>
					</table>
				</div>
			</div>
<? include "./footer.php"; ?>