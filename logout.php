<? session_start(); ?>

<?
	if(!isset($_SESSION['id']))
	{
		echo "
			<script>
				alert('잘못된 경로로 접근하였습니다.');
				history.back(-1);
			</script>
			";
	}
	else
	{
		if(isset($_SESSION['admin'])) $logout = "admin.php";
		else  $logout = "index.php";
		 
		session_destroy();
		$_SESSION = array();
		
		echo "
			<script>
				location.href='./".$logout."';
			</script>
			";
	}
?>

<? include "./main.php"; ?>