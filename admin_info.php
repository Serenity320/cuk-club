<? include "./admin_chk.php"; ?>
<? include "./db_connect.php"; ?>

<?
	if($open) $open_print = "접속 가능";
	else $open_print = "접속 불가능";
?>

<? include "./header.php"; ?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">관리자 정보</h1>
				<div class="table-responsive">
					<script>
						function chkPasswd()
						{
							var form = document.frm_paaawd;

							if(form.passwd_before.value.length == 0)
							{
								alert('현재 비밀번호를 입력하세요.');
								form.passwd_before.focus();
								return false;
							}
							
							if(form.passwd_after1.value.length == 0)
							{
								alert('변경할 비밀번호를 입력하세요.');
								form.passwd_after1.focus();
								return false;
							}
							
							if(form.passwd_after1.value.length < 4)
							{
								alert('비밀번호는 4~10자리로 입력하세요.');
								form.passwd_after1.focus();
								return false;
							}
							
							if(form.passwd_after2.value.length == 0)
							{
								alert('변경할 비밀번호를 입력하세요.');
								form.passwd_after2.focus();
								return false;
							}
							
							if(form.passwd_after2.value.length < 4)
							{
								alert('비밀번호는 4~10자리로 입력하세요.');
								form.passwd_after2.focus();
								return false;
							}
							
							if(form.passwd_after1.value != form.passwd_after2.value)
							{
								alert('변경할 비밀번호가 일치하지 않습니다.');
								form.passwd_after1.focus();
								return false;
							}
							
							return true;
						}
					</script>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th width="20%">현재 비밀번호</th>
								<td width="30%">
									<form name="frm_paaawd" action="./admin_modify.php" method="post" onsubmit="return chkPasswd()">
										<input type="password" name="passwd_before" value="" size="15" maxlength="10" />
								</td>
								<th width="50%">※ 현재 사용 중인 비밀번호 입력</th>
							</tr>
							<tr>
								<th>비밀번호 변경</th>
								<td>
										<input type="password" name="passwd_after1" value="" size="15" maxlength="10" />
								</td>
								<th>※ 비밀번호는 4~10자리로 입력</th>
							</tr>
							<tr>
								<th>비밀번호 변경 확인</th>
								<td>
										<input type="password" name="passwd_after2" value="" size="15" maxlength="10" />
								</td>
								<th>※ 변경할 비밀번호 재입력</th>
							</tr>
							<tr>
								<td colspan="3" align="center">
										<input type="hidden" name="mod_type" value="passwd" />
										<button type="submit" class="btn btn-sm btn-default">비밀번호 변경</button>
									</form>
								</td>
							</tr>
							<tr>
								<td colspan="3"></td>
							</tr>
							<tr>
								<th>동아리 접속 활성화</th>
								<td><?=$open_print?></td>
								<td>
									<form name="frm_open" action="./admin_modify.php" method="post">
										<input type="hidden" name="mod_type" value="open" />
										<button type="submit" class="btn btn-sm btn-default">접속 활성화 변경</button>
									</form>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
<? include "./footer.php"; ?>