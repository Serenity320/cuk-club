<? include "./index_chk.php"; ?>
<? include "./db_connect.php"; ?>

<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">
		
		<title><?=$title?></title>
		
		<!-- Bootstrap core CSS -->
		<link href="./dist/css/bootstrap.min.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link href="./css/signin.css" rel="stylesheet">
		
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="./assets/js/ie-emulation-modes-warning.js"></script>
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body>
		
		<div class="container">
			
			<center><img src="./images/logo.jpg" height="200" width="200"></center>
			<form class="form-signin" role="form" action="./admin_ok.php" method="post">
				<input type="id" name="id" class="form-control" placeholder="아이디" required autofocus>
				<input type="password" name="password" class="form-control" placeholder="비밀번호" required>
				<button class="btn btn-lg btn-primary btn-block" type="submit">관리자 로그인</button>
			</form>
		
		</div> <!-- /container -->
		
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="./assets/js/ie10-viewport-bug-workaround.js"></script>
	</body>
	
	<footer class="footer">
			<table border="0" align="center">
				<tr>
					<td rowspan="2" width="180" align="center"><img src="./images/cuk.gif" width="177" height="42"></td>
					<td rowspan="2" width="70" align="center"><img src="./images/logo.jpg" width="50" height="50"></td>
					<td align="center"><b>&copy; 마/음/을/비/추/는/그/린/라/이/트/! /31/대/총/동/아/리/연/합/회</b></td>
				</tr>
				<tr>
					<td align="center"><b>Last Update : 2014.11.21 / Made by. KSU</b></td>
				</tr>
			</table>
		</footer>
</html>