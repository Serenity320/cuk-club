<? include "./admin_chk.php"; ?>
<? include "./db_connect.php"; ?>

<?
	$query = "select * from club where clb_id = '".$_POST['clb_id']."'";
	$result1 = mysql_query($query, $connect) or die(mysql_error());
	$row1 = mysql_fetch_array($result1);
	
	$query = "select count(*) as cnt from member where clb_id = '".$row1[clb_id]."' and mbr_leave_check = false";
	$result2 = mysql_query($query, $connect) or die(mysql_error());
	$row2 = mysql_fetch_array($result2);
	
	$query = "select count(*) as cnt from member where clb_id = '".$row1[clb_id]."' and mbr_vos_check = false";
	$result3 = mysql_query($query, $connect) or die(mysql_error());
	$row3 = mysql_fetch_array($result3);
?>

<? include "./header.php"; ?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">동아리 목록 > 관리</h1>
				<div class="table-responsive">
					<script>
						function chkName()
						{
							var form = document.frm_name;
							
							if(form.clb_name.value.length == 0)
							{
								alert('동아리명을 입력하세요.');
								form.clb_name.focus();
								return false;
							}
							
							if(form.clb_name.value == '<?=$row1[clb_name]?>')
							{
								alert('현재 동아리명과 동일합니다.');
								form.clb_name.focus();
								return false;
							}
							
							return true;
						}
						
						function chkDelete()
						{
							var form = document.frm_delete;
							
							if(<?=$row2[cnt]?> > 0)
							{
								alert('동아리 회원을 모두 탈퇴 후 삭제해 주시기 바랍니다.');
								return false;
							}
							
							if(<?=$row3[cnt]?> > 0)
							{
								alert('V.O.S 제출 확인 후 삭제할 수 있습니다.');
								return false;
							}
							
							return true;
						}
					</script>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th width = "20%">동아리명</th>
								<td width = "30%">
									<form name="frm_name" action="./club_modify.php" method="post" onsubmit="return chkName()">
										<input type="text" name="clb_name" value="<?=$row1[clb_name]?>" maxlength="10" />
								</td>
								<td width = "50%">
										<input type="hidden" name="mod_type" value="name" />
										<input type="hidden" name="clb_id" value="<?=$row1[clb_id]?>" />
										<button type="submit" class="btn btn-sm btn-default">수정</button>
									</form>
								</td>
							</tr>
							<tr>
								<th>동아리 코드</th>
								<td><?=$row1[clb_code]?></td>
								<td>
									<form name="frm_code" action="./club_modify.php" method="post">
										<input type="hidden" name="mod_type" value="code" />
										<input type="hidden" name="clb_id" value="<?=$row1[clb_id]?>" />
										<button type="submit" class="btn btn-sm btn-default">초기화</button>
									</form>
								</td>
							</tr>
							<tr>
								<th>회원 수</th>
								<td><?=number_format($row2[cnt]);?> 명</td>
								<td>
									<form name="frm_delete" action="./club_delete.php" method="post" onsubmit="return chkDelete()">
										<input type="hidden" name="clb_id" value="<?=$row1[clb_id]?>" />
										<button type="submit" class="btn btn-sm btn-default">동아리 삭제</button>
									</form>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
<? include "./footer.php"; ?>