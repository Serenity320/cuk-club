<? session_start(); ?>

<?
	if(isset($_SESSION['id']) && !isset($_SESSION['admin']))
	{
		echo "
			<script>
				alert('로그아웃 후 이용하시기 바랍니다.');
				location.href='./member.php';
			</script>
			";
	}
	else if(!(isset($_SESSION['id']) && isset($_SESSION['admin'])))
	{
		echo "
			<script>
				alert('로그인이 필요합니다.');
				location.href='./admin.php';
			</script>
			";
	}
?>