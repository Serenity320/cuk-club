<? include "./admin_chk.php"; ?>
<? include "./db_connect.php"; ?>

<?
	$query = "select count(*) as cnt from club";
	$result = mysql_query($query, $connect) or die(mysql_error());
	$row = mysql_fetch_array($result);
	
	function selected($type, $value)
	{
		if($_POST[$type] == $value) return " selected";
	}
?>

<? include "./header.php"; ?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">동아리 목록</h1>
				<div class="table-responsive">
					<table class="table table-striped">
						<tr>
							<td width="50%">
								<form name="frm_search_type1" action="./club.php" method="post">
									<select name="type1">
										<option value="club_name"<?=selected("type1", "club_name");?>>동아리명</option>
									</select>
									<input type="text" name="keyword" value="<?=$_POST['keyword']?>" size="15" />
									<button type="submit" class="btn btn-xs btn-default">검색</button>
								</form>
							</td>
							
							<td width="30%">
								<form name="frm_add" action="./club_add.php" method="post">
									<button type="submit" class="btn btn-sm btn-default">동아리 등록</button>
								</form>
							</td>
							<th width="20%">[ 동아리 수 : <?=number_format($row[cnt]);?> 개 ]</th>
						</tr>
					</table>
				</div>
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>동아리명</th>
								<th>동아리 코드</th>
								<th>회원 수</th>
								<th>정보</th>
								<th>관리</th>
							</tr>
						</thead>
						<tbody>
<?
	$query = "select * from club";
	if($_POST['type1'] == "club_name") $query .= " where clb_name like '%".$_POST['keyword']."%'";
	
	$result1 = mysql_query($query, $connect) or die(mysql_error());
	while($row1 = mysql_fetch_array($result1))
	{
		$query = "select count(*) as cnt from member where clb_id = '".$row1[clb_id]."' and mbr_leave_check = false";
		$result2 = mysql_query($query, $connect) or die(mysql_error());
		$row2 = mysql_fetch_array($result2);
?>
							<tr>
								<td><?=$row1[clb_name]?></td>
								<td><?=$row1[clb_code]?></td>
								<td><?=number_format($row2[cnt]);?> 명</td>
								<td>
									<form name="frm_info" action="./club_info.php" method="post">
										<input type="hidden" name="clb_code" value="<?=$row1[clb_code]?>" />
										<button type="submit" class="btn btn-sm btn-default">정보</button>
									</form>
								</td>
								<td>
									<form name="frm_manage" action="./club_manage.php" method="post">
										<input type="hidden" name="clb_id" value="<?=$row1[clb_id]?>" />
										<button type="submit" class="btn btn-sm btn-default">관리</button>
									</form>
								</td>
								<td>
									
								</td>
							</tr>
<?
	}
?>
						</tbody>
					</table>
				</div>
			</div>
<? include "./footer.php"; ?>