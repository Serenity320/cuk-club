<? session_start(); ?>
<?
	if(isset($_SESSION['id']) && isset($_SESSION['admin']))
	{
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8");
		header( "Content-Disposition: attachment; filename = cukclub_vos.xls" );
		header( "Content-Description: PHP4 Generated Data" );
	}
?>

<? include "./db_connect.php"; ?>

<?
	if(isset($_SESSION['id']) && isset($_SESSION['admin']))
	{
		$query = "select * from member, club, student where member.stu_num = student.stu_num and member.clb_id = club.clb_id";
		$query = $query." and member.mbr_vos_check = false order by member.mbr_leave_date asc";
		$result = mysql_query($query, $connect) or die(mysql_error());
		
		$excel = "
			<table border='1'>  
				<tr>  
					<td align='center'><font size='3'><b>학번</b></font></td>
					<td align='center'><font size='3'><b>이름</b></font></td>
					<td align='center'><font size='3'><b>동아리명</b></font></td>
					<td align='center'><font size='3'><b>직책</b></font></td>
					<td align='center'><font size='3'><b>연락처</b></font></td>
					<td align='center'><font size='3'><b>가입일</b></font></td>
					<td align='center'><font size='3'><b>탈퇴일</b></font></td> 
				</tr>
			";
		  
		while($row = mysql_fetch_array($result))
		{ 
			$excel .= "  
				<tr>
					<td align='center'><font size='3'>".$row[stu_num]."</font></td>
					<td align='center'><font size='3'>".$row[stu_name]."</font></td>
					<td align='center'><font size='3'>".$row[clb_name]."</font></td>
					<td align='center'><font size='3'>".$row[mbr_pos]."</font></td>
					<td align='center'><font size='3'>".$row[stu_contact]."</font></td>
					<td align='center'><font size='3'>".$row[mbr_join_date]."</font></td>
					<td align='center'><font size='3'>".$row[mbr_leave_date]."</font></td>
				</tr>";  
		}  
		  
		$excel .= "</table>";
		
		
	}
	else
	{
		echo "
			<script>
				alert('잘못된 경로로 접근하였습니다.');
				history.back(-1);
			</script>
			";
	}
?>

<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8">
		<title><?=$title?></title>
	</head>
	<body>
		<?=$excel?>
	</body>
</html>