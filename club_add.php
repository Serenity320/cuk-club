<? include "./admin_chk.php"; ?>
<? include "./db_connect.php"; ?>

<?
	function selected($type, $value)
	{
		if($_GET[$type] == $value) return "selected";
	}
?>

<? include "./header.php"; ?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">동아리 등록</h1>
				<div class="table-responsive">
					<script>
						function chkAdd()
						{
							var form = document.frm_add;
							
							if(form.clb_name.value.length == 0)
							{
								alert('동아리명을 입력하세요.');
								form.clb_name.focus();
								return false;
							}
							
							return true;
						}
					</script>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th width = "20%">동아리명</th>
								<td width = "30%">
									<form name="frm_add" action="./club_add_ok.php" method="post" onsubmit="return chkAdd()">
										<input type="text" name="clb_name" value="" size="15" />
								</td>
								<td width = "50%">
							</tr>
							<tr>
								<th>동아리 코드</th>
								<td>동아리 등록 후 확인</td>
								<td>
										<button type="submit" class="btn btn-sm btn-default">등록</button>
									</form>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
<? include "./footer.php"; ?>