<? include "./admin_chk.php"; ?>
<? include "./db_connect.php"; ?>

<?
	$query = "select * from student where stu_num = '".$_POST[stu_num]."'";
	$result1 = mysql_query($query, $connect) or die(mysql_error());
	$row1 = mysql_fetch_array($result1);
	
	$query = "select count(*) as cnt from member where stu_num = '".$row1[stu_num]."' and mbr_leave_check = false";
	$result2 = mysql_query($query, $connect) or die(mysql_error());
	$row2 = mysql_fetch_array($result2);
	
	$query = "select count(*) as cnt from member where stu_num = '".$row1[stu_num]."' and mbr_vos_check = false";
	$result3 = mysql_query($query, $connect) or die(mysql_error());
	$row3 = mysql_fetch_array($result3);
	
	$contact = explode("-", $row1[stu_contact]);
?>

<? include "./header.php"; ?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">학생 목록 > 관리</h1>
				<div class="table-responsive">
					<script>
						function chkContact()
						{
							var number = /[^0-9]/;
							var form = document.frm_contact;
							
							if(form.stu_contact1.value.length == 0)
							{
								alert('연락처를 입력하세요.');
								form.stu_contact1.focus();
								return false;
							}
							
							if(form.stu_contact1.value.match(number) != null)
							{
								alert('연락처는 숫자만 입력이 가능합니다.');
								form.stu_contact1.value = "";
								form.stu_contact1.focus();
								return false;
							}
							
							if(form.stu_contact2.value.length == 0)
							{
								alert('연락처를 입력하세요.');
								form.stu_contact2.focus();
								return false;
							}
							
							if(form.stu_contact2.value.match(number) != null)
							{
								alert('연락처는 숫자만 입력이 가능합니다.');
								form.stu_contact2.value = "";
								form.stu_contact2.focus();
								return false;
							}
							
							if(form.stu_contact3.value.length == 0)
							{
								alert('연락처를 입력하세요.');
								form.stu_contact3.focus();
								return false;
							}
							
							if(form.stu_contact3.value.match(number) != null)
							{
								alert('연락처는 숫자만 입력이 가능합니다.');
								form.stu_contact3.value = "";
								form.stu_contact3.focus();
								return false;
							}
							
							return true;
						}
						
						function chkDelete()
						{
							var form = document.frm_delete;
							
							if(<?=$row2[cnt]?> > 0)
							{
								alert('동아리를 모두 탈퇴 후 삭제해 주시기 바랍니다.');
								return;
							}
							
							if(<?=$row3[cnt]?> > 0)
							{
								alert('V.O.S 제출 확인 후 삭제할 수 있습니다.');
								return;
							}
							
							form.submit();
						}
					</script>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th width = "20%">학번</th>
								<td width = "30%"><?=$row1[stu_num]?></td>
								<td width = "50%"</td>
							</tr>
							<tr>
								<th>이름</th>
								<td><?=$row1[stu_name]?></td>
								<td></td>
							</tr>
							<tr>
								<th>연락처</th>
								<td>
									<form name="frm_contact" action="./student_modify.php" method="post" onsubmit="return chkContact()">
										<input type="text" name="stu_contact1" value="<?=$contact[0]?>" size="3" maxlength="4" />-
										<input type="text" name="stu_contact2" value="<?=$contact[1]?>" size="3" maxlength="4" />-
										<input type="text" name="stu_contact3" value="<?=$contact[2]?>" size="3" maxlength="4" />
								</td>
								<td>
										<input type="hidden" name="mod_type" value="contact" />
										<input type="hidden" name="stu_num" value="<?=$row1[stu_num]?>" />
										<button type="submit" class="btn btn-sm btn-default">수정</button>
									</form>
								</td>
							</tr>
							<tr>
								<th>동아리 가입 수</th>
								<td><?=number_format($row2[cnt]);?> 개</td>
								<td>
									<form name="frm_delete" action="./student_delete.php" method="post" onsubmit="return chkDelete()">
										<input type="hidden" name="stu_num" value="<?=$row1[stu_num]?>" />
										<button type="submit" class="btn btn-sm btn-default">학생 삭제</button>
									</form>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
<? include "./footer.php"; ?>