<? include "./admin_chk.php"; ?>
<? include "./db_connect.php"; ?>

<?
	$query = "select count(*) as cnt from member";
	$result = mysql_query($query, $connect) or die(mysql_error());
	$row = mysql_fetch_array($result);
	
	function selected($type, $value)
	{
		if($_POST[$type] == $value) return "selected";
	}
	
	function bool_print($value)
	{
		if($value) return "O";
		else return "X";
	}
?>

<? include "./header.php"; ?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">회원 목록</h1>
				<div class="table-responsive">
					<table class="table table-striped">
						<tr>
							<td width="30%">
								<form name="frm_search_type1" action="./member.php" method="post">
									<select name="type1">
										<option value="stu_num"<?=selected("type1", "stu_num");?>>학번</option>
										<option value="stu_name"<?=selected("type1", "stu_name");?>>이름</option>
										<option value="club_name"<?=selected("type1", "club_name");?>>동아리명</option>
									</select>
									<input type="text" name="keyword" value="<?=$_POST['keyword']?>" size="15" />
									<input type="hidden" name="type2" value="<?=$_POST['type2']?>" />
									<input type="hidden" name="sort" value="<?=$_POST['sort']?>" />
									<input type="hidden" name="type3" value="<?=$_POST['type3']?>" />
									<button type="submit" class="btn btn-xs btn-default">검색</button>
								</form>
							</td>
							<td width="30%">
								<form name="frm_search_type2" action="./member.php" method="post">
									<input type="hidden" name="type1" value="<?=$_POST['type1']?>" />
									<input type="hidden" name="keyword" value="<?=$_POST['keyword']?>" />
									<select name="type2">
										<option value="stu_num"<?=selected("type2", "stu_num");?>>학번</option>
										<option value="stu_name"<?=selected("type2", "stu_name");?>>이름</option>
										<option value="club_name"<?=selected("type2", "club_name");?>>동아리명</option>
									</select>
									<select name="sort">
										<option value="asc"<?=selected("sort", "asc");?>>오름차순</option>
										<option value="desc"<?=selected("sort", "desc");?>>내림차순</option>
									</select>
									<input type="hidden" name="type3" value="<?=$_POST['type3']?>" />
									<button type="submit" class="btn btn-xs btn-default" type="submit">정렬</button>
								</form>
							</td>
							<td width="20%">
								<form name="frm_search_type3" action="./member.php" method="post">
									<input type="hidden" name="type1" value="<?=$_POST['type1']?>" />
									<input type="hidden" name="keyword" value="<?=$_POST['keyword']?>" />
									<input type="hidden" name="type2" value="<?=$_POST['type2']?>" />
									<input type="hidden" name="sort" value="<?=$_POST['sort']?>" />
									<select name="type3">
										<option value="except"<?=selected("type3", "except");?>>탈퇴자 제외</option>
										<option value="include"<?=selected("type3", "include");?>>탈퇴자 포함</option>
									</select>
								<button type="submit" class="btn btn-xs btn-default" type="submit">선택</button>
								</form>
							</td>
							<th width="20%">[ 총 회원 수 : <?=number_format($row[cnt]);?> 명 ]</th>
						</tr>
					</table>
				</div>
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>학번</th>
								<th>이름</th>
								<th>동아리명</th>
								<th>직책</th>
								<th>연락처</th>
								<th>가입일</th>
								<th>탈퇴일</th>
								<th>인계확인</th>
								<th>인계일</th>
								<th>관리</th>
							</tr>
						</thead>
						<tbody>
<?
	$query = "select * from member, club, student where member.stu_num = student.stu_num and member.clb_id = club.clb_id";
	
	if($_POST['type1'] == "stu_num") $query .= " and student.stu_num like '%".$_POST['keyword']."%'";
	else if($_POST['type1'] == "stu_name") $query .= " and student.stu_name like '%".$_POST['keyword']."%'";
	else if($_POST['type1'] == "club_name") $query .= " and club.clb_name like '%".$_POST['keyword']."%'";
	
	if($_POST['type3'] != "include") $query .= " and member.mbr_leave_check = false";
	
	if($_POST['type2'] == "stu_name") $query .= " order by student.stu_name ".$_POST['sort'];
	else if($_POST['type2'] == "club_name") $query .= " order by club.clb_name ".$_POST['sort'];
	else $query .= " order by member.mbr_leave_date asc, student.stu_num ".$_POST['sort'];
	
	$result = mysql_query($query, $connect) or die(mysql_error());
	while($row = mysql_fetch_array($result))
	{
?>
							<tr>
								<td><?=$row[stu_num]?></td>
								<td><?=$row[stu_name]?></td>
								<td><?=$row[clb_name]?></td>
								<td><?=$row[mbr_pos]?></td>
								<td><?=$row[stu_contact]?></td>
								<td><?=$row[mbr_join_date]?></td>
								<td><?=$row[mbr_leave_date]?></td>
								<td><?=bool_print($row[mbr_vos_check]);?></td>
								<td><?=$row[mbr_vos_date]?></td>
								<td>
									<form name="frm_manage" action="./member_manage.php" method="post">
										<input type="hidden" name="mbr_id" value="<?=$row[mbr_id]?>" />
										<button type="submit" class="btn btn-sm btn-default">관리</button>
									</form>
								</td>
							</tr>
<?
	}
?>
						</tbody>
					</table>
				</div>
			</div>
<? include "./footer.php"; ?>