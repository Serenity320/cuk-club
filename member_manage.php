<? include "./admin_chk.php"; ?>
<? include "./db_connect.php"; ?>

<?
	$query = "select * from member, club, student where member.mbr_id = '".$_POST['mbr_id']."' and member.stu_num = student.stu_num and member.clb_id = club.clb_id";
	$result = mysql_query($query, $connect) or die(mysql_error());
	$row = mysql_fetch_array($result);
?>

<? include "./header.php"; ?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">동아리 목록 > 관리</h1>
				<div class="table-responsive">
					<script>
						function chkDelete()
						{
							var form = document.frm_delete;
							
							if(!<?=$row[mbr_vos_check]?>)
							{
								alert('V.O.S 제출 확인 후 회원탈퇴가 가능합니다.');
								return false;
							}
							
							return true;
						}
					</script>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th width = "20%">학번</th>
								<td width = "30%"><?=$row[stu_num]?></td>
								<td width = "50%"></td>
							</tr>
							<tr>
								<th>이름</th>
								<td><?=$row[stu_name]?></td>
								<td></td>
							</tr>
							<tr>
								<th>동아리명</th>
								<td><?=$row[clb_name]?></td>
								<td></td>
							</tr>
							<tr>
								<th>직책</th>
<?
	if($row[mbr_leave_check])
	{
?>
								<td><?=$row[mbr_pos]?></td>
								<td></td>
<?
	}
	else
	{
?>
								<td>
									<form name="frm_position" action="./member_modify.php" method="post">
										<input type="text" name="mbr_pos" value="<?=$row[mbr_pos]?>" maxlength="8" />
								</td>
								<td>
										<input type="hidden" name="mod_type" value="position" />
										<input type="hidden" name="mbr_id" value="<?=$row[mbr_id]?>" />
										<button type="submit" class="btn btn-sm btn-default">수정</button>
									</form>
								</td>
<?
	}
?>
							</tr>
							<tr>
								<th>연락처</th>
								<td><?=$row[stu_contact]?></td>
								<td></td>
							</tr>
							<tr>
								<th>가입일</th>
								<td><?=$row[mbr_join_date]?></td>
								<td></td>
							</tr>
							<tr>
								<th>탈퇴일</th>
<?
	if($row[mbr_leave_check])
	{
?>
								<td><?=$row[mbr_leave_date]?></td>
								<td>
<?
		if(!$row[mbr_vos_check])
		{
?>
									<form name="frm_return" action="./member_modify.php" method="post">
										<input type="hidden" name="mod_type" value="return" />
										<input type="hidden" name="mbr_id" value="<?=$row[mbr_id]?>" />
										<button type="submit" class="btn btn-sm btn-default">탈퇴 취소</button>
									</form>
<?
		}
?>
								</td>
<?
	}
	else
	{
?>
								<td>
									<form name="frm_delete" action="./member_modify.php" method="post" onsubmit="return chkDelete()">
										<select name="mbr_join_year">
<?
		for($i = -2; $i <= 2; $i++)
		{
			$year = date("Y") - $i;
			
			if($year == date("Y")) $selected = " selected";
			else $selected = "";
?>
											<option value="<?=$year?>"<?=$selected?>><?=$year?></option>
<?
	}
?>
										</select>년 
										<select name="mbr_join_month">
<?
		for($i = 1; $i <= 12; $i++)
		{
			$month = $i;
			
			if($month == date("m")) $selected = " selected";
			else $selected = "";
?>
											<option value="<?=$month?>"<?=$selected?>><?=$month?></option>
<?
		}
?>
										</select>월 
										<select name="mbr_join_day">
<?
		for($i = 1; $i <= 31; $i++)
		{
			$day = $i;
			
			if($day == date("d")) $selected = " selected";
			else $selected = "";
?>
											<option value="<?=$day?>"<?=$selected?>><?=$day?></option>
<?
		}
?>
										</select>일
								</td>
								<td>
										<input type="hidden" name="mod_type" value="delete" />
										<input type="hidden" name="mbr_id" value="<?=$row[mbr_id]?>" />
										<button type="submit" class="btn btn-sm btn-default">동아리 탈퇴</button>
									</form>
								</td>
<?
	}
?>
							</tr>
<?
	if(!$row[mbr_leave_check] && !$row[mbr_vos_check])
	{
?>
							<tr>
								<td colspan="3" align="center">
									<form name="frm_cancle" action="./member_modify.php" method="post">
										<input type="hidden" name="mod_type" value="cancle" />
										<input type="hidden" name="mbr_id" value="<?=$row[mbr_id]?>" />
										<button type="submit" class="btn btn-sm btn-default">가입 취소</button>
									</form>
								</td>
							</tr>
<?
	}
?>
						</tbody>
					</table>
				</div>
			</div>
<? include "./footer.php"; ?>