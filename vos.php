<? include "./admin_chk.php"; ?>
<? include "./db_connect.php"; ?>

<? include "./header.php"; ?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">V.O.S 제출</h1>
				<div class="table-responsive">
					<table class="table table-striped">
						<tr>
							<td width="10%">
								<form name="frm_excel" action="./vos_excel.php" method="post">
									<input type="hidden" name="vos" value="excel" />
									<button type="submit" class="btn btn-sm btn-default">① 엑셀 다운로드</button>
								</form>
							</td width="90%">
							<td>
								<form name="frm_vos" action="./vos_submit.php" method="post">
									<input type="hidden" name="vos" value="submit" />
									<button type="submit" class="btn btn-sm btn-default">② V.O.S 제출 확인</button>
								</form>
							</td>
							<td width="10%">
								<form name="frm_vos" action="./vos_submit.php" method="post">
									<input type="hidden" name="vos" value="restore" />
									<button type="submit" class="btn btn-sm btn-default">V.O.S 제출 취소</button>
								</form>
							</td>
						</tr>
				<div class="table-responsive">
					</table>
				</div>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>학번</th>
								<th>이름</th>
								<th>동아리명</th>
								<th>직책</th>
								<th>연락처</th>
								<th>가입일</th>
								<th>탈퇴일</th>
							</tr>
						</thead>
						<tbody>
<?
	$query = "select * from member, club, student where member.stu_num = student.stu_num and member.clb_id = club.clb_id";
	$query .= " and member.mbr_vos_check = false order by member.mbr_leave_date asc, club.clb_name asc";
	$result = mysql_query($query, $connect);
	while($row = mysql_fetch_array($result))
	{
?>
							<tr>
								<td><?=$row[stu_num]?></td>
								<td><?=$row[stu_name]?></td>
								<td><?=$row[clb_name]?></td>
								<td><?=$row[mbr_pos]?></td>
								<td><?=$row[stu_contact]?></td>
								<td><?=$row[mbr_join_date]?></td>
								<td><?=$row[mbr_leave_date]?></td>
							</tr>
<?
	}
?>
						</tbody>
					</table>
				</div>
			</div>
<? include "./footer.php"; ?>