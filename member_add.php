<? include "./admin_chk.php"; ?>
<? include "./db_connect.php"; ?>

<?
	if($_SERVER['REQUEST_METHOD'] == "POST")
	{
		if($_POST['stu_num'])
		{
			$query = "select * from student where stu_num = '".$_POST['stu_num']."'";
			$result = mysql_query($query, $connect) or die(mysql_error());
			$row = mysql_fetch_array($result);
			
			if($row) $stu_exist = "yes";
			else $stu_exist = "no";
		}
	}
?>

<? include "header.php"; ?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">회원 등록</h1>
				<div class="table-responsive">
					<script>
						function chkSearch_stu()
						{
							var number = /[^0-9]/;
							var form = document.frm_secrch_stu;
							
							if(form.stu_num.value.length == 0)
							{
								alert('학번을 입력하세요.');
								form.stu_num.focus();
								return false;
							}
							
							if(form.stu_num.value.match(number) != null)
							{
								alert('학번은 숫자만 입력이 가능합니다.');
								form.stu_num.value = "";
								form.stu_num.focus();
								return false;
							}
							
							return true;
						}
						
						function chkSearch_clb()
						{
							var form = document.frm_secrch_clb;
							
							if(form.clb_name.value.length == 0)
							{
								alert('동아리명을 입력하세요.');
								form.stu_num.focus();
								return false;
							}
							
							return true;
						}
						
						function chkAdd()
						{
							var number = /[^0-9]/;
							var form = document.frm_add;
							
							if("<?=$_POST['stu_num']?>" == "")
							{
								alert('학번을 검색하세요.');
								form.stu_num.focus();
								return false;
							}
							
							if(form.clb_id.value.length == 0)
							{
								alert('동아리를 검색한 후에 선택하세요.');
								form.clb_name.focus();
								return false;
							}
							
							if("<?=$stu_exist?>" == "no")
							{
								if(form.stu_name.value.length == 0)
								{
									alert('이름을 입력하세요.');
									form.stu_name.focus();
									return false;
								}
								
								if(form.stu_name.value.match(number) == null)
								{
									alert('이름은 문자만 입력이 가능합니다.');
									form.stu_name.value = "";
									form.stu_name.focus();
									return false;
								}
								
								if(form.stu_contact1.value.length == 0)
								{
									alert('연락처를 입력하세요.');
									form.stu_contact1.focus();
									return false;
								}
								
								if(form.stu_contact1.value.match(number) != null)
								{
									alert('연락처는 숫자만 입력이 가능합니다.');
									form.stu_contact1.value = "";
									form.stu_contact1.focus();
									return false;
								}
								
								if(form.stu_contact2.value.length == 0)
								{
									alert('연락처를 입력하세요.');
									form.stu_contact2.focus();
									return false;
								}
								
								if(form.stu_contact2.value.match(number) != null)
								{
									alert('연락처는 숫자만 입력이 가능합니다.');
									form.stu_contact2.value = "";
									form.stu_contact2.focus();
									return false;
								}
								
								if(form.stu_contact3.value.length == 0)
								{
									alert('연락처를 입력하세요.');
									form.stu_contact3.focus();
									return false;
								}
								
								if(form.stu_contact3.value.match(number) != null)
								{
									alert('연락처는 숫자만 입력이 가능합니다.');
									form.stu_contact3.value = "";
									form.stu_contact3.focus();
									return false;
								}
							}
							
							return true;
						}
					</script>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th width="20%">학번 검색</th>
								<td width="30%">
									<form name="frm_secrch_stu" action="./member_add.php" method="post" onsubmit="return chkSearch_stu();">
										<input type="text" name="stu_num" value="" size="15" maxlength="9" />
										<input type="hidden" name="clb_name" value="<?=$_POST['clb_name']?>" />
										<button type="submit" class="btn btn-xs btn-default">검색</button>
									</form>
								</td>
								<th width="50%">※ 학생의 학번을 검색하세요.</th>
							</tr>
							<tr>
								<th>동아리 검색</th>
								<td>
									<form name="frm_secrch_clb" action="./member_add.php" method="post" onsubmit="return chkSearch_clb();">
										<input type="hidden" name="stu_num" value="<?=$_POST['stu_num']?>" />
										<input type="text" name="clb_name" value="" size="15" maxlength="8" />
										<button type="submit" class="btn btn-xs btn-default">검색</button>
									</form>
								</td>
								<th>※ 동아리명을 검색하세요.</th>
							</tr>
							<tr>
								<td colspan="3"></td>
							</tr>
							<tr>
								<th width="20%">학번</th>
								<td width="30%">
									<?=$_POST['stu_num']?>
									<form name="frm_add" action="./member_add_ok.php" method="post" onsubmit="return chkAdd()">
										<input type="hidden" name="stu_num" value="<?=$_POST['stu_num']?>" />
								</td>
								<th><font color="#ff0000"><? if($stu_exist == "no") { ?>※ 등록되지 않은 학번입니다. 정보를 입력하면 학생이 등록됩니다.<? } ?></font></th>
							</tr>
							<tr>
								<th>이름</th>
								
<?
	if($stu_exist == "yes")
	{
?>
								<td><?=$row[stu_name]?></td>
<?
	}
	else if($stu_exist == "no")
	{
?>
								<td>
										<input type="text" name="stu_name" value="" size="15"  maxlength="8" />
								</td>
<?
	}
	else
	{
?>
								<td></td>
<?
	}
?>
								<td></td>
							</tr>
							<tr>
								<th>연락처</th>
<?
	if($stu_exist == "yes")
	{
?>
								<td><?=$row[stu_contact]?></td>
<?
	}
	else if($stu_exist == "no")
	{
?>
								<td>
										<input type="text" name="stu_contact1" value="" size="3" maxlength="4" /> -
										<input type="text" name="stu_contact2" value="" size="3" maxlength="4" /> -
										<input type="text" name="stu_contact3" value="" size="3" maxlength="4" />
								</td>
<?
	}
	else
	{
?>
								<td></td>
<?
	}
?>
								<td></td>
							</tr>
							<tr>
								<td colspan="3"></td>
							</tr>
							<tr>
								<th>동아리명</th>
								<td>
										<select name="clb_id">
<?
	if($_POST['clb_name'])
	{
		$query = "select * from club where clb_name like '%".$_POST['clb_name']."%'";
		$result2 = mysql_query($query, $connect) or die(mysql_error());
		while($row2 = mysql_fetch_array($result2))
		{
			$check = true;
?>
											<option value="<?=$row2[clb_id]?>"><?=$row2[clb_name]?></option>
<?
		}
	}
?>
										</select>
								</td>		
<?
	if($check)
	{
?>
								<th>※ 동아리를 선택하세요.</th>
<?
	}
	else if($_POST['clb_name'])
	{
?>
								<th><font color="#ff0000">※ 동아리 검색 결과가 없습니다.</font></th>
<?
	}
?>
							</tr>
							<tr>
								<th>직책</th>
								<td>
										<input type="text" name="mbr_pos" value="" size="15" maxlength="8" />
								</td>
								<td></td>
							</tr>
							<tr>
								<th>가입일</th>
								<td>
										<select name="mbr_join_year">
<?
	for($i = 1990; $i <= date("Y"); $i++)
	{
		if($i == date("Y")) $selected = " selected";
		else $selected = "";
?>
											<option value="<?=$i?>"<?=$selected?>><?=$i?></option>
<?
	}
?>
										</select>년 
										<select name="mbr_join_month">
<?
	for($i = 1; $i <= 12; $i++)
	{
		if($i == date("m")) $selected = " selected";
		else $selected = "";
?>
											<option value="<?=$i?>"<?=$selected?>><?=$i?></option>
<?
	}
?>
										</select>월 
										<select name="mbr_join_day">
<?
	for($i = 1; $i <= 31; $i++)
	{
		if($i == date("d")) $selected = " selected";
		else $selected = "";
?>
											<option value="<?=$i?>"<?=$selected?>><?=$i?></option>
<?
	}
?>
										</select>일
								</td>
								<td></td>
							</tr>
							<tr>
								<td colspan="3" align="center">
										<button type="submit" class="btn btn-sm btn-default">등록</button>
									</form>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
<? include "footer.php"; ?>