<? session_cache_limiter('no-cache, must-revalidate'); ?>
<? include "./login_chk.php"; ?>
<? include "./db_connect.php"; ?>

<?
	if(isset($_SESSION['admin']) && isset($_POST['clb_code'])) $code = $_POST['clb_code'];
	else if(!isset($_SESSION['admin']) && isset($_SESSION['id'])) $code = $_SESSION['id'];
	else
	{
		echo "
			<script>
				alert('잘못 된 경로로 접속하셨습니다.');
				history.back(-1);
			</script>
			";
	}
	
	$query = "select * from club where clb_code = '".$code."'";
	$result1 = mysql_query($query, $connect) or die(mysql_error());
	$row1 = mysql_fetch_array($result1);
	
	$query = "select count(*) as cnt from member where clb_id = '".$row1[clb_id]."' and mbr_leave_check = false";
	$result2 = mysql_query($query, $connect) or die(mysql_error());
	$row2 = mysql_fetch_array($result2);
?>

<?
	function selected($type, $value)
	{
		if($_POST[$type] == $value) return "selected";
	}
	
	function bool_print($value)
	{
		if($value) return "O";
		else return "X";
	}
?>

<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title><?=$title?></title>

		<!-- Bootstrap core CSS -->
		<link href="./dist/css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="./css/dashboard.css" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="./assets/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body>
		
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">CUK CLUB</a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#"><?=$row1[clb_name]?></a></li>
						<li><a href="logout.php">[로그아웃]</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3 col-md-2 sidebar">
					<ul class="nav nav-sidebar">
						<li><a href="#">회원 목록</a></li>
<?
	if($_SESSION["admin"])
	{
?>
						<li><a href="./club.php">동아리 목록</a></li>
<?
	}
?>
					</ul>
				</div>
				<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
					<h1 class="page-header">회원 목록 > <?=$row1[clb_name]?></h1>
					<div class="table-responsive">
						<table class="table table-striped">
							<tr>
								<td width="30%">
									<form name="frm_search_type1" action="./club_info.php" method="post">
<?
	if($_SESSION["admin"])
	{
?>
										<input type="hidden" name="clb_code" value="<?=$_POST["clb_code"]?>" />
<?
	}
?>
										<select name="type1">
											<option value="stu_num" <?=selected("type1", "stu_num");?>>학번</option>
											<option value="stu_name" <?=selected("type1", "stu_name");?>>이름</option>
										</select>
										<input type="text" name="keyword" value="<?=$_POST["keyword"]?>" size="15" />
										<input type="hidden" name="type2" value="<?=$_POST["type2"]?>" />
										<input type="hidden" name="sort" value="<?=$_POST["sort"]?>" />
										<button type="submit" class="btn btn-xs btn-default">검색</button>
									</form>
								</td>
								<td width="50%">
									<form name="frm_search_type2" action="./club_info.php" method="post">
<?
	if($_SESSION["admin"])
	{
?>
										<input type="hidden" name="clb_code" value="<?=$_POST["clb_code"]?>" />
<?
	}
?>
										<input type="hidden" name="type1" value="<?=$_POST["type1"]?>" />
										<input type="hidden" name="keyword" value="<?=$_POST["keyword"]?>" />
										<select name="type2">
											<option value="stu_num" <?=selected("type2", "club_name");?>>학번</option>
											<option value="stu_name" <?=selected("type2", "club_name");?>>이름</option>
										</select>
										<select name="sort">
											<option value="asc" <?=selected("sort", "asc");?>>오름차순</option>
											<option value="desc" <?=selected("sort", "desc");?>>내림차순</option>
										</select>
										<button type="submit" class="btn btn-xs btn-default">정렬</button>
									</form>
								</td width="20%">
								<th>동아리 회원 : <?=number_format($row2[cnt]);?> 명</th>
							</tr>
						</table>
					</div>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>학번</th>
									<th>이름</th>
									<th>직책</th>
									<th>연락처</th>
									<th>가입일</th>
									<th>인계확인</th>
									<th>인계일</th>
								</tr>
							</thead>
							<tbody>
<?
	$query = "select * from member, club, student where member.stu_num = student.stu_num and member.clb_id = club.clb_id";
	if($code) $query .= " and club.clb_code = '".$code."'";
	
	if($_POST["type1"] == "stu_num") $query .= " and student.stu_num like '%".$_POST["keyword"]."%'";
	else if($_POST["type1"]=="stu_name") $query .= " and student.stu_name like '%".$_POST["keyword"]."%'";
	
	$query .= " and member.mbr_leave_check = false";
	
	if($_POST["type2"] == "stu_name") $query .= " order by student.stu_name ".$_POST["sort"];
	else $query .= " order by student.stu_num ".$_POST["sort"];
	
	$result = mysql_query($query, $connect) or die(mysql_error());
	while($row = mysql_fetch_array($result))
	{
?>
								<tr>
									<td><?=$row[stu_num]?></td>
									<td><?=$row[stu_name]?></td>
									<td><?=$row[mbr_pos]?></td>
									<td><?=$row[stu_contact]?></td>
									<td><?=$row[mbr_join_date]?></td>
									<td><?=bool_print($row[mbr_vos_check]);?></td>
									<td><?=$row[mbr_vos_date]?></td>
								</tr>
<?
	}
?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<footer class="footer">
			<table border="0" align="center">
				<tr>
					<td rowspan="2" width="180" align="center"><img src="./images/cuk.gif" width="177" height="42"></td>
					<td rowspan="2" width="70" align="center"><img src="./images/logo.jpg" width="50" height="50"></td>
					<td align="center"><b>&copy; 마/음/을/비/추/는/그/린/라/이/트/! /31/대/총/동/아/리/연/합/회</b></td>
				</tr>
				<tr>
					<td align="center"><b>Last Update : 2014.11.21 / Made by. KSU</b></td>
				</tr>
			</table>
		</footer>
		
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="./dist/js/bootstrap.min.js"></script>
		<script src="./assets/js/docs.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="./assets/js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html>