<? include "./admin_chk.php"; ?>
<? include "./db_connect.php"; ?>

<? include "./header.php"; ?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">공지사항 관리</h1>
				<script type="text/javascript" src="./js/HuskyEZCreator.js" charset="utf-8"></script>
				<form name="frm_save" action="./notice_save.php" method="post">
					<textarea name="ir1" id="ir1" rows="10" cols="100" style="width:766px; height:412px; display:nofnene;"></textarea>
					<p>
						<button type="submit" class="btn btn-default" onclick="submitContents(this);">저장</button>
					</p>
				</form>
				<script type="text/javascript">
					var oEditors = [];
								
					// 추가 글꼴 목록
					//var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];
					
					nhn.husky.EZCreator.createInIFrame({
						oAppRef: oEditors,
						elPlaceHolder: "ir1",
						sSkinURI: "SmartEditor2Skin.html",	
						htParams : {
							bUseToolbar : true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
							bUseVerticalResizer : true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
							bUseModeChanger : true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
							//aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
							fOnBeforeUnload : function(){
								//alert("완료!");
							}
						}, //boolean
						fOnAppLoad : function(){
							//예제 코드
							var sHTML = "<?=$notice?>";
							oEditors.getById["ir1"].exec("PASTE_HTML", [sHTML]);
						},
						fCreator: "createSEditor2"
					});
					function submitContents(elClickedObj) {
						oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.
						
						// 에디터의 내용에 대한 값 검증은 이곳에서 document.getElementById("ir1").value를 이용해서 처리하면 됩니다.
						
						try {
							elClickedObj.form.submit();
						} catch(e) {}
					}
				</script>
			</div>
<? include "./footer.php"; ?>